<?php

foreach (glob('Models/*.php') as $file) {
    include_once($file);
}

foreach (glob('Controllers/*.php') as $file) {
    include_once($file);
}

include_once('Views/View.php');

$view = new View();

$view->displayHeader();

$controller_get = isset($_GET['controller']) ? $_GET['controller'] : "";

switch ($controller_get) {
    case 'article':
        if (isset($_GET['id_article']) && Article::exists($id_article = (int)$_GET['id_article'])) {
            (new ArticleController())->displayArticle($id_article);
        } elseif (!isset($_GET['id_article']) && isset($_GET['new_article'])) {
            (new ArticleController())->newArticle();
        } else {
            (new HomeController())->displayArticles();
        }
        break;

    default:
        $article_page = (isset($_GET['page']) ? (int)$_GET['page'] : 1);
        $id_category = (isset($_GET['id_category']) ? (int)$_GET['id_category'] : 0);
        (new HomeController())->displayArticles($article_page, $id_category);
        break;
}

$view->displayFooter();
