<div class="container">
    <?php
        foreach ($this->errors as $error) {
            echo "<div class='alert alert-danger'>" . $error . "</div>";
        }
    ?>
    <div class="row">
        <form class="col-lg-12" id="filter" method="get">
            <div class="offset-lg-9 col-lg-3">
                <select id="id_category" class="form-control" name="id_category">
                    <option value='0'>Toutes les catégories</option>
                    <?php
                        foreach ($this->vars['categories'] as $category) {
                            if (isset($_GET['id_category']) && $_GET['id_category'] == $category['id_category']) {
                                echo "<option value='" . $category['id_category'] . "' selected>" . $category['name'] . "</option>";
                            } else {
                                echo "<option value='" . $category['id_category'] . "'>" . $category['name'] . "</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </form>
        <script type="text/javascript">
            $('#id_category').on('change', function() {
                $('#filter').submit();
            })
        </script>
    </div>
    <?php foreach ($this->vars['articles'] as $article) { ?>
        <div class="row" style="background-color:rgb(200,200,200); margin:50px 0">
            <div class="col-md-3">

                <?php
                    if (file_exists($article['image'])) {
                        echo "<img src='" . Db::clean($article['image']) . "' style='max-height:150px;max-width:150px;'>";
                    }
                ?>
            </div>
            <div class="col-md-9">
                <?php
                    echo "<h2>" . htmlspecialchars($article['title']) . "</h2>";
                    echo "<p>" . htmlspecialchars($article['resume']) . "</p>";
                    echo "<a href='index.php?controller=article&id_article=" . (int)$article['id_article'] . "'>Voir l'article</a>";
                ?>
            </div>
        </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <?php
                if ($this->vars['page'] > 1) {
                    echo "<a class='btn btn-light' href='index.php?page=" . ($this->vars['page'] - 1) . $this->vars['filter_url'] . "'>Page précédente</a>";
                }
                if ($this->vars['nb_pages'] > $this->vars['page']) {
                    echo "<a class='btn btn-light' href='index.php?page=" . ($this->vars['page'] + 1) . $this->vars['filter_url'] . "'>Page suivante</a>";
                }
            ?>
        </div>
    </div>
</div>
