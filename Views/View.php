<?php

class View
{
    // Tableau de variables
    public $vars = array();

    // Tableau contenant toutes les erreurs
    public $errors = array();

    // Tableau contenant tous les succès
    public $successes = array();

    /**
    * Affiche l'en tête
    */
    public function displayHeader()
    {
        include_once("Views/header.php");
    }

    /**
     * Affiche le ifchier voulu
     *
     * @param string file : chemin vers le fichier à afficher
     */
    public function display($file)
    {
        if ($file == null) {
            return false;
        }
        include_once($file);
    }

    /**
    * Affiche le pied de page
    */
    public function displayFooter()
    {
        include_once("Views/footer.php");
    }
}
