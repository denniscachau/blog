<?php $article = $this->vars['article']; ?>

<div class="container">
    <?php
        foreach ($this->errors as $error) {
            echo "<div class='alert alert-danger'>" . $error . "</div>";
        }

        foreach ($this->successes as $success) {
            echo "<div class='alert alert-success'>" . $success . "</div>";
        }
    ?>
    <form method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                            if (file_exists($article['image'])) {
                                echo "<img class='img-fluid' src='" . Db::clean($article['image']) . "'>";
                            }
                        ?>
                    </div>
                </div>

                <div class="to_display" style="display:none">
                    <div class="row">
                        <div class="col-lg-12">
                            <select class="form-control" name="id_category">
                                <?php foreach ($this->vars['categories'] as $category) {
                                    if ($category['id_category'] == $article['id_category']) {
                                        echo "<option value='" . (int) $category['id_category'] . "' selected>" . $category['name'] . "</option>";
                                    } else {
                                        echo "<option value='" . (int) $category['id_category'] . "'>" . $category['name'] . "</option>";
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <?php echo "<h1 id='title'>" . htmlspecialchars($article['title']) . "</h1>"; ?>
                </div>

                <div class="row">
                    <?php echo "<p id='text'>" . htmlspecialchars($article['text']) . "</p>"; ?>
                </div>

                <div class="row to_display" style="display:none">
                    <label>Résumé :</label>
                    <?php echo "<textarea maxlength='255' class='form-control' name='resume'>" . $article['resume'] . "</textarea>"; ?>
                </div>
            </div>
        </div>



        <?php echo "<input type='hidden' name='id_article' value='" . (int)$article['id_article'] . "'>"; ?>
        <div class="row">
            <button id="button_modify" class="btn btn-primary" type="button">Modifier</button>
            <button id="button_submit" class="btn btn-primary" style="display:none" type="submit" name="modify_article" value="modify">Valider</button>
            <button class="btn btn-danger" type="submit" name="modify_article" value="delete">Supprimer</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('document').ready(function() {
        $('form').on('click', '#button_modify', function() {
            let title = $('#title').text();
            $('#title').replaceWith("<label>Titre :</label><input maxlength='63' class='form-control form-control-lg' type='text' name='title' id='title'>");
            $('#title').val(title);

            let text = $('#text').text();
            $('#text').replaceWith("<label>Texte :</label><textarea class='form-control' name='text' id='text'>" + text + "</textarea>");

            $('#button_modify').remove();
            $('#button_submit').show();
            $('.to_display').show();
        });
    })
</script>
