<div class="container">

    <?php
        foreach ($this->errors as $error) {
            echo "<div class='alert alert-danger'>" . $error . "</div>";
        }

        foreach ($this->successes as $success) {
            echo "<div class='alert alert-success'>" . $success . "</div>";
        }
    ?>

    <form method="post" enctype="multipart/form-data" class="col-lg-6">
        <div class="form-group">
            <label for="title">Titre :</label><input maxlength="63" type="text" name="title" id="title" placeholder="Titre" class="form-control">
        </div>
        <div class="form-group">
            <label for="resume">Catégorie :</label>
            <select class="form-control" name="id_category">
                <?php foreach ($this->vars['categories'] as $category) {
                    echo "<option value='" . (int) $category['id_category'] . "'>" . $category['name'] . "</option>";
                } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="resume">Résumé :</label><input maxlength="255" type="text" name="resume" id="resume" placeholder="Résumé" class="form-control">
        </div>
        <div class="form-group">
            <label for="text">Texte :</label><textarea id="text" name="text" placeholder="Texte" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label for="image">Image : </label><input id="image" type="file" accept="image/*" name="image">
        </div>

        <div class="form-group">
            <input class="btn btn-light" type="submit" name="add_article" value="Ajouter">
        </div>
    </form>
</div>
