<?php

class Db
{
    private static $db;

    /**
     * Retourne l'instance de la base de donneés
     *
     * @return PDO
     */
    private static function getInstance()
    {
        try {
            static::$db = new PDO('mysql:host=127.0.0.1;dbname=cachau_blog;charset=utf8', 'root', '');

            return static::$db;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Retourne la requête sous forme de tableau
     *
     * @param string sql : requete à exécuter (de type select)
     *
     * @return array
     */
    public static function executeS($sql)
    {
        try {
            $response = static::getInstance()->query($sql);
            $row = 0;
            $result = array();

            // Pour chaque lignes de la réponse SQL
            while ($datas = $response->fetch()) {
                $column = 0;
                // Pour chaque champs
                foreach ($datas as $key => $data) {
                    if ($column % 2 == 0) {
                        $result[$row][$key] = $data;
                    }
                    $column++;
                }
                $row++;
            }
            $response->closeCursor();
            if (!empty($result)) {
                return $result;
            } else {
                return array();
            }
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * Execute la requete
     *
     * @param string sql : requete à exécuter (de type insert, update, delete)
     *
     * @return bool
     */
    public static function execute($sql)
    {
        try {
            static::getInstance()->exec($sql);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Retourne la première valeur de la requête
     *
     * @param string sql : requete à exécuter (de type select)
     *
     * @return string
     */
    public static function getValue($sql)
    {
        $return = static::executeS($sql . " LIMIT 1");
        if (is_array($return) && isset($return[0])) {
            return $return[0][array_keys($return[0])[0]];
        } else {
            return false;
        }
    }

    /**
     * Evite les injections SQL
     *
     * @param string string : chaîne à "nettoyer"
     *
     * @return string
     */
    public static function clean($string)
    {
        $string = stripslashes($string);

        $string = str_replace(array('\\', "\n", "'", '"'), array('\\\\', '\\n', "\'", '\"'), $string);

        return $string;
    }
}
