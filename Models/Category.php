<?php

class Category
{
    /**
     * Retourne toutes les catégories
     *
     * @return array
     */
    public static function getCategories()
    {
        return Db::executeS("SELECT * FROM `categories`");
    }
}
