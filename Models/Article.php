<?php

class Article
{
    // Tous les champs de la table articles (sans la clé primaire)
    private $fields = array(
        'id_category' => array(),
        'title' => array(
            'size' => 64
        ),
        'image' => array(),
        'resume' => array(
            'size' => 255
        ),
        'text' => array()
    );

    // Nombre d'articles affichés par page
    private const NB_ARTICLES = 10;

    /**
     * Est-ce que l'article existe
     *
     * @param int id : id de l'article
     *
     * @return bool
     */
    public static function exists(int $id)
    {
        return (bool)Db::getValue("SELECT `id_article` FROM `articles` WHERE `id_article` = '" . $id . "'");
    }

    /**
     * Ajoute un article (en utilisant les reqêtes POST)
     *
     * @return bool
     */
    public function add()
    {
        if (!static::importImage()) {
            return false;
        }
        $insert = array();
        foreach ($this->fields as $key => $field) {
            if (!isset($_POST[$key]) || empty($_POST[$key]) || (isset($field['size']) && $field['size'] < strlen($_POST[$key]))) {
                return false;
            } else {
                $insert['keys'][] = "`" . Db::clean($key) . "`";
                $insert['values'][] = "'" . Db::clean($_POST[$key]) . "'";
            }
        }

        // if (isset($_FILES['image'])) {
        //     // code...
        // }

        $query = "INSERT INTO `articles` (" . implode(', ', $insert['keys']) . ") VALUES (" . implode(', ', $insert['values']) . ")";

        return (bool) Db::execute($query);
    }

    /**
     * Met à jour un article (en utilisant les reqêtes POST)
     *
     * @return bool
     */
    public function update()
    {
        if (!isset($_POST['id_article']) || !static::exists($id_article = (int) $_POST['id_article'])) {
            return false;
        }

        $update = array();
        foreach ($this->fields as $key => $field) {
            if (isset($_POST[$key]) && !empty($_POST[$key]) && (!isset($field['size']) || $field['size'] >= strlen($_POST[$key]))) {
                $update[] = "`" . Db::clean($key) . "` = '" . Db::clean($_POST[$key]) . "'";
            }
        }
        if (!empty($update)) {
            $query = "UPDATE `articles` SET " . implode(', ', $update) . " WHERE `id_article` = '" . $id_article . "'";

            return (bool) Db::execute($query);
        }

        return false;
    }

    /**
     * Supprime un article (en utilisant les reqêtes POST)
     *
     * @return bool
     */
    public function delete()
    {
        if (!isset($_POST['id_article']) || !static::exists($id_article = (int) $_POST['id_article'])) {
            return false;
        }
        $image = Db::getValue("SELECT `image` FROM `articles` WHERE `id_article` = '" . $id_article . "'");

        if (file_exists($image)) {
            unlink($image);
        }

        $query = "DELETE FROM `articles` WHERE `id_article` = '" . $id_article . "'";

        return (bool) Db::execute($query);
    }

    /**
     * Importe l'image d'un article
     *
     * @return bool
     */
    private static function importImage()
    {
        $_POST['image'] = "";
        if (isset($_FILES['image'])) {
            $type = $_FILES['image']['type'];

            if (preg_match('/image\/.*/', $type)) {
                $type = explode('.', $_FILES['image']['name']);
                $type = $type[count($type) - 1];
                $filename = date('s') * rand(1, 50);
                $target_file = "images/" . (int)($filename) . "." . $type;
                $_POST['image'] = $target_file;
                return move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
            }
        }
        return false;
    }

    /**
     * Retourne les données d'un article
     *
     * @param int id : ID de l'article
     *
     * @return array
     */
    public static function getArticle(int $id)
    {
        if (!static::exists($id)) {
            return false;
        }

        $article_query = Db::executeS(
            "SELECT *
            FROM `articles` `a` LEFT JOIN `categories` `c` ON `a`.`id_category` = `c`.`id_category`
            WHERE `id_article` = '" . $id . "'"
        );
        if (!empty($article_query)) {
            return $article_query[0];
        } else {
            return array();
        }
    }

    /**
     * Retourne tous les articles
     *
     * @param int page : numéro de la page
     * @param int id_category : ID de la catégorie (filtre)
     *
     * @return array
     */
    public static function getArticles(int $page = 1, int $id_category = 0)
    {
        $limit = static::NB_ARTICLES * ($page - 1);
        $limit = " LIMIT " . $limit . ", " . (static::NB_ARTICLES);

        $where = $id_category > 0 ? " AND `a`.`id_category` = '" . $id_category . "'" : "";

        return Db::executeS(
            "SELECT *
            FROM `articles` `a` LEFT JOIN `categories` `c` ON `a`.`id_category` = `c`.`id_category`
            WHERE 1" . $where . "
            ORDER BY `a`.`id_article` DESC" . $limit
        );
    }

    /**
     * Retourne le nombre de page en fonction du nombre d'articles et du nombre d'articles que l'on affiche par page
     *
     * @param int page : numéro de la page
     * @param int id_category : ID de la catégorie (filtre)
     *
     * @return int
     */
    public static function getNbPages(int $page = 1, int $id_category = 0)
    {
        $where = $id_category > 0 ? " AND `id_category` = '" . $id_category . "'" : "";

        $nb_categories = (int) Db::getValue(
            "SELECT COUNT(`id_article`)
            FROM `articles`
            WHERE 1" . $where
        );

        return (int) ($nb_categories / static::NB_ARTICLES) + 1;
    }
}
