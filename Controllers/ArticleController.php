<?php

class ArticleController
{
    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * Détails d'un article
     */
    public function displayArticle(int $id)
    {
        if (!Article::exists($id)) {
            return false;
        }

        if (isset($_POST['modify_article'])) {
            $article_obj = new Article();
            switch ($_POST['modify_article']) {
                case 'modify':
                    if ($article_obj->update()) {
                        $this->view->successes[] = "Mise à jour réussie";
                    } else {
                        $this->view->errors[] = "Une erreur est survenue";
                    }
                    break;
                case 'delete':
                    if ($article_obj->delete()) {
                        header('Location: index.php');
                    } else {
                        $this->view->errors[] = "Une erreur est survenue";
                    }
                    break;
            }
        }

        $this->view->vars = array(
            'article' => Article::getArticle($id),
            'categories' => Category::getCategories()
        );

        $this->view->display('Views/Article/article.php');
    }

    /**
     * Ajout d'un article
     */
    public function newArticle()
    {
        if (isset($_POST['add_article'])) {
            $article = new Article();

            if ($article->add()) {
                $this->view->successes[] = "Ajout réussie";
            } else {
                $this->view->errors[] = "Une erreur est survenue";
            }
        }
        $this->view->vars = array(
            'categories' => Category::getCategories()
        );

        $this->view->display('Views/Article/new_article.php');
    }
}
