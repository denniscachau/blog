<?php

class HomeController
{
    private $view;

    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * Affichage de tous les articles
     */
    public function displayArticles(int $page = 1, int $id_category = 0)
    {
        $articles = Article::getArticles($page, $id_category);
        if (empty($articles)) {
            $this->view->errors[] = "Aucun article";
        }
        $this->view->vars = array(
            'articles' => $articles,
            'page' => $page,
            'nb_pages' => Article::getNbPages($page, $id_category),
            'filter_url' => $id_category == 0 ? "" : "&id_category=" . $id_category,
            'categories' => Category::getCategories()
        );

        $this->view->display('Views/Home/home.php');
    }
}
